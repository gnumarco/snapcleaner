package com.snaprapid.snapcleaner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import cmu.arktweetnlp.Twokenize;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

/**
 *
 * @author marc
 */
public class TextCleaner {

    HashMap<String, String> abbrDict = new HashMap<>();

    private void loadDict(String dict) {
        try {
            Reader in = new FileReader(dict);
            Iterable<CSVRecord> records = CSVFormat.DEFAULT.withHeader().withDelimiter('%').parse(in);
            for (CSVRecord csvRecord : records) {
                abbrDict.put(csvRecord.get(0).toLowerCase(), csvRecord.get(1).toLowerCase());
            }
            System.out.println(abbrDict);
        } catch (FileNotFoundException ex) {
            System.err.println(ex);
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }

    private String cleanTags(String text) {
        String cleanStr = "";

        if (text != null) {
            String[] words = text.split(" ");
            for (String w : words) {
                if (!(w.startsWith("#") || w.startsWith("@"))) {
                    cleanStr += w;
                    cleanStr += " ";
                } else if (w.startsWith("@")) {
                    cleanStr += w.substring(1);
                    cleanStr += " ";
                }

//                if (w.startsWith("#") || w.startsWith("@")) {
//                    cleanStr += w.substring(1);
//                    cleanStr += " ";
//                } else {
//                    cleanStr += w;
//                    cleanStr += " ";
//                }
            }
        }
        return cleanStr.trim();
    }

    private String cleanHTTP(String text) {
        String cleanStr = "";

        if (text != null) {
            String[] words = text.split(" ");
            for (String w : words) {
                if (!w.startsWith("http")) {

                    cleanStr += w;
                    cleanStr += " ";
                }
            }
        }
        return cleanStr.trim();
    }

    private String cleanAbbrv(String text) {
        String cleanStr = "";

        if (text != null) {
            String[] words = text.split(" ");
            //String[] words = text.split("\\W+");
            for (String w : words) {
                if (abbrDict.containsKey(w.toLowerCase())) {
                    cleanStr += abbrDict.get(w.toLowerCase());
                    cleanStr += " ";
                } else {
                    cleanStr += w;
                    cleanStr += " ";
                }
            }
        }
        return cleanStr.trim();
    }

    private String cleanFull(List<String> text) {
        String cleanStr = "";
        for (String w : text) {
            if (!w.startsWith("http")) {
                if ((w.startsWith("#") || w.startsWith("@"))) {
                    
                    w= w.substring(1);

                }
                if (abbrDict.containsKey(w.toLowerCase())) {
                    w = abbrDict.get(w.toLowerCase());
                } 
                cleanStr +=w+" ";
            }
            
        }
        cleanStr = cleanStr.trim();
       

        return cleanStr.trim();
    }
    
    public String clean(String text){
        return cleanFull(Twokenize.tokenizeRawTweetText(text));
    }
    
    public TextCleaner(){
        loadDict("abbrDict.csv");
        loadDict("iconsDict.csv");
    }

    // Just to test
    public static void main(String[] args) {
        TextCleaner tc = new TextCleaner();
        tc.loadDict("abbrDict.csv");
        tc.loadDict("iconsDict.csv");
        
        
        String testTxt = "This is 😢 some text ❤ 😊 http://myvideo.com wtf!! LOL ;) #thisisahashtag @thisisausername";
        System.out.println(Twokenize.tokenizeRawTweetText(testTxt));
        System.out.println(tc.cleanFull(Twokenize.tokenizeRawTweetText(testTxt)));
    }
}
